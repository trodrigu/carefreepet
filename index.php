<?php
	// Define the root folder and base URL for the application
	function baseURL(){
		return sprintf(
			"%s://%s%s",
			isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
			$_SERVER['SERVER_NAME'],
			dirname($_SERVER['REQUEST_URI'])
		);
	}

	define('BASE_URL', baseURL());
	define('KY_ROOT', dirname(__FILE__));
	$page = isset($_GET['page']) ? $_GET['page'] : 'home';
	
	$pages = array(
		'home' => 'Home',
		'category' => 'Category - Grid/List',
		'detail' => 'Detail',
		'cart' => 'Shopping Cart',
		'checkout' => 'Checkout',
		'contact' => 'Contact Us',
		'404' => '404'
		);
?>
<!DOCTYPE html>
	<html lang="en">
	<head>
		<!-- Meta -->
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">

		<title>Carefree Pet</title>

		<!-- Bootstrap Core CSS -->
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">

		<!-- Customizable CSS -->
		<link rel="stylesheet" href="assets/css/main.css">
		<link href="assets/css/purple.css" rel="stylesheet" title="Purple color">
		<link rel="stylesheet" href="assets/css/bootstrap-select.min.css">
		<link rel="stylesheet" href="assets/css/owl.carousel.css">
		<link rel="stylesheet" href="assets/css/owl.transitions.css">
		<link rel="stylesheet" href="assets/css/animate.min.css">

		<!-- Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Asap:400,700,400italic,700italic' rel='stylesheet' type='text/css'>

		<!-- Icons/Glyphs -->
		<link rel="stylesheet" href="assets/css/elegant-fonts.css">

		<!-- Favicon -->
		<link rel="shortcut icon" href="assets/images/fav_logo_v3.ico">

		<!-- HTML5 elements and media queries Support for IE8 : HTML5 shim and Respond.js -->
		<!--[if lt IE 9]>
		<script src="assets/js/html5shiv.js"></script>
		<script src="assets/js/respond.min.js"></script>
		<![endif]-->
    </head>
<body>


	<div class="wrapper">
		<?php require KY_ROOT.'/parts/section/header.php';?>

		<?php require_once KY_ROOT.'/pages/'.$page.'.php'; ?>

		<?php require KY_ROOT.'/parts/section/footer.php';?>  
	</div><!-- /.wrapper -->

	<!-- JavaScripts placed at the end of the document so the pages load faster -->

	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="assets/js/jquery-1.11.0.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/bootstrap-select.min.js"></script>
	<script src="assets/js/bootstrap-hover-dropdown.min.js"></script>
	<script src="assets/js/bootstrap-slider.js"></script>
	<script src="assets/js/echo.min.js"></script>
	<script src="assets/js/jquery.easing-1.3.min.js"></script>
	<script src="assets/js/flatui-controls.js"></script>
	<script src="assets/js/owl.carousel.min.js"></script>
	<script src="assets/js/wow.min.js"></script>
	<script src="assets/js/scripts.js"></script>

</body>
</html>