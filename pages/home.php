<div class="content">
	<?php require KY_ROOT.'/parts/section/home-page-slider.php' ?>

	<div class="container">
		<div class="page-section">
			<p>
				For over ten years, our online natural pet store has served our customers the optimal premium holistic dog foods, natural cat foods, dog vitamins, and a wide ranging assortment of targeted natural pet health care products. Our selection of natural pet care products incorporate the practices of pet homeopathy, holistic pet care, pet herbal remedies, and variety of supplementary natural pet health philosophies that promote the overall health and wellness of your dog, cat, or horse.

				The Carefree Pet specializes in natural pet care products including premium natural dog food, supplements, vitamins, holistic cat foods, horse vitamin supplements and a wide variety of natural pet health supplies.

				It is our pleasure to supply pet parents with the finest premium natural pet supplies on the market!

			</p>
		</div>
	</div>
	<div class="container">
		<div class="page-section wow fadeInUp">
		<div class="row">
		  <div class="col-sm-6 col-md-4">
		    <div class="thumbnail">
		      <img src="http://www.carefreepet.com/productimages/mega_pet_daily_125.jpg" data-src="holder.js/300x300" alt="...">
		      <div class="caption">
		        <h3>Featured Item For Dogs</h3>
		        <p>Azmira's Mega Pet Daly is the most effective multiple nutritional supplement available to date.</p>
		        <p><a href="#" class="btn btn-primary" role="button">Learn More</a></p>
		      </div>
		    </div>
		  </div>
		  <div class="col-sm-6 col-md-4">
		    <div class="thumbnail">
		      <img src="http://www.carefreepet.com/productimages/zoomin_catnip_125.jpg" data-src="holder.js/300x300" alt="...">
		      <div class="caption">
		        <h3>Featured Item For Cats</h3>
		        <p>Zoomin Catnip is wild crafted, pesticide-free catnip that provideds fun and emotional wellbeing for your cat.</p>
		        <p><a href="#" class="btn btn-primary" role="button">Learn More</a></p>
		      </div>
		    </div>
		  </div>
		  <div class="col-sm-6 col-md-4">
		    <div class="thumbnail">
		      <img src="http://www.carefreepet.com/productimages/yucca_intensive_8oz_125.jpg" data-src="holder.js/300x300" alt="...">
		      <div class="caption">
		        <h3>Featured Item For Horses</h3>
		        <p>Natural Steroid Alternative!  Azmira's Yucca Intensive reduces pain and inflammation as well as steroids, bute and aspirin without gastric side-effect.</p>
		        <p><a href="#" class="btn btn-primary" role="button">Learn More</a></p>
		      </div>
		    </div>
		  </div>
		</div>
		</div>
	</div>
</div><!-- /.content -->