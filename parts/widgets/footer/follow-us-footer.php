<!-- ============================================================= FOLLOW US FOOTER ============================================================= -->
<div class="module widget">
    <div class="module-header">
        <h4 class="module-title text-md-center">Follow Us</h4>
    </div><!-- /.module-header -->
    <div class="module-body text-md-center">
        <div class="row">
            <div class="col-md-12 col-sm-6">
                <p>We don't bite.</p>
                <ul class="list-social-icons">
                    <li><a href="http://facebook.com/carefreepet"><span data-icon="&#xe093;"></span></a></li>
                    <li><a href="#"><span data-icon="&#xe094;"></span></a></li>
<!--                     <li><a href="#"><span data-icon="&#xe095;"></span></a></li>
                    <li><a href="#"><span data-icon="&#xe096;"></span></a></li>
                    <li><a href="#"><span data-icon="&#xe09b;"></span></a></li>
 -->                </ul><!-- /.list-social-icons -->
            </div><!-- /.col -->
            <div class="col-md-12 col-sm-6">
                <address>
                    <strong>The Carefree Pet</strong><br/>
                    6781 Outlook Drive, Tucson, AZ 85756 <br/>
                    Toll-free: 888-595-6061 
                </address>  
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.module-body -->
</div><!-- /.module -->
<!-- ============================================================= FOLLOW US FOOTER : END ============================================================= -->