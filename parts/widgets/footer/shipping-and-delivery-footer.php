<!-- ============================================================= SHIPPING DELIVERY FOOTER ============================================================= -->
<div class="module">
    <div class="module-header">
        <h4 class="module-title text-md-center">Shipping &amp; Delivery</h4>
    </div><!-- /.module-header -->
    <div class="module-body text-md-center">
        <ul class="list-link">
            <li><a href="index.php?page=category">Free shipping for orders over $59.</a></li>
            <li><a href="index.php?page=category">Same day domesic shipping before 4:00pm.</a></li>
            <li><a href="index.php?page=category">At this time, The Carefree Pet is unable to fulfill international orders.</a></li>
            <li><a href="index.php?page=category">Returns are accepted within 30 days of receipt of the order.</a></li>
            <li><a href="index.php?page=category">Damaged orders must be called in within 5 days.</a></li>
        </ul><!-- /.list-link -->
    </div><!-- /.module-body -->
</div><!-- /.module -->
<!-- ============================================================= SHIPPING DELIVERY FOOTER :END ============================================================= -->