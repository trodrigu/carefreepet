<!-- ============================================================= NAVBAR FOOTER ============================================================= -->
<div class="container text-md-center">
	<ul class="navbar-nav nav hidden-sm hidden-xs">
		<li><a href="index.php?page=home">Home</a></li>
		<li><a href="index.php?page=contact">Contact</a></li>
		<li><a href="index.php?page=checkout">Shipping</a></li>
	</ul><!-- /.navbar-nav -->
	<p class="navbar-text pull-right text-md-center copyright-text">copyright &copy; 2015 Carefree Pet</p>
</div><!-- /.container -->
<!-- ============================================================= NAVBAR FOOTER : END ============================================================= -->
