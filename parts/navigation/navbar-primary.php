<!-- ============================================================= NAVBAR PRIMARY ============================================================= -->

<nav class="yamm navbar navbar-primary animate-dropdown" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button id="btn-navbar-primary-collapse" type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-primary-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div><!-- /.navbar-header -->
        <div class="collapse navbar-collapse" id="navbar-primary-collapse">
            <ul class="nav navbar-nav">
                <li><a href="index.php">Home</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">Products</a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="yamm-content">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <span class="megamenu-title">Dogs</span>
                                        <ul class="menu-items">
                                            <li><a href="index.php?page=category">Natural Dog Care</a></li>
                                            <li><a href="index.php?page=category">Natural Dog Foods</a></li>
                                            <li><a href="index.php?page=category">For Dog Health Problems</a></li>
                                        </ul>
                                        <span class="megamenu-title">Cats</span>
                                        <ul class="menu-items">
                                            <li><a href="index.php?page=category">Natural Cat Care</a></li>
                                            <li><a href="index.php?page=category">Natural Cat Food</a></li>
                                            <li><a href="index.php?page=category">For Cat Health Problems</a></li>
                                        </ul>
                                        <span class="megamenu-title">Horses</span>
                                        <ul class="menu-items">
                                            <li><a href="index.php?page=category">Natural Horse Care</a></li>
                                            <li><a href="index.php?page=category">For Horse Health Problems</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">About Us</a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="yamm-content">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <span class="megamenu-title">Reach out to us</span>
                                        <ul class="menu-items">
                                            <li><a href="index.php?page=contact">Support</a></li>
                                            <li><a href="index.php?page=contact">Contact Us</a></li>
                                            <li><a href="index.php?page=contact">Newsletter</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
                <li><a href="index.php?page=cart">Articles</a></li>
                <li><a href="index.php?page=cart">View Order</a></li>
            </ul><!-- /.nav -->
            
        </div><!-- /.collapse navbar-collapse -->
    </div><!-- /.container -->
</nav><!-- /.yamm -->
<!-- ============================================================= NAVBAR PRIMARY : END ============================================================= -->