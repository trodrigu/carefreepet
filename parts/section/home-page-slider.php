<!-- ========================================== SECTION – HERO ========================================= -->
			
<div id="hero">
	<div id="owl-main" class="owl-carousel home-page-carousel height-lg owl-inner-nav owl-ui-lg owl-theme">
		<div class="item" style="background-image: url(assets/images/sliders/flyingdog.jpg);">
			<div class="container">
				<div class="caption vertical-center text-center">
					<div class="big-text fadeInDown-1" style="opacity: 0;">
						A carefree dog is a healthy dog.
					</div>
					<div class="fadeInDown-2 text hidden-xs" style="opacity: 0;">
					  Prevention is the best cure.  <br> We have over a thousand solutions for everything from allergies to immunodeficiencies. 
					</div>
<!-- 					<div class="button-holder fadeInDown-3" style="opacity: 0;">
					    <a class="btn uppercase strong btn-secondary btn-add-to-cart btn-action" href="index.php?page=detail">Add To Cart</a>
					</div>
 -->					<!-- /.button-holder -->    

				</div><!-- /.caption --> 
			</div><!-- /.container --> 
		</div><!-- /.item -->


		<div class="item" style="background-image: url(assets/images/sliders/dogpullstick.jpg);">
			<div class="container">
				<div class="caption vertical-center text-center">
					<div class="big-text fadeInDown-1" style="opacity: 0;">
						Yucca Intensive <br> Pain relief
					</div>
					<div class="fadeInDown-2 text hidden-xs" style="opacity: 0;">
					  With over 85% active saponins you can be assured of its effectiveness. <br> Reduces inflammation associated with arthritis, injuries and even allergies!
					</div>
<!-- 					<div class="button-holder fadeInDown-3" style="opacity: 0;">
					    <a class="btn uppercase strong btn-secondary btn-add-to-cart btn-action" href="index.php?page=detail">Add To Cart</a>
					</div>
 -->					<!-- /.button-holder -->      
				</div><!-- /.caption -->   
			</div><!-- /.container --> 
		</div><!-- /.item -->
		
		<div class="item" style="background-image: url(assets/images/sliders/bigdog.jpg);">
			<div class="container">
				<div class="caption vertical-center text-center">
					<div class="big-text fadeInDown-1" style="opacity: 0;">
						Megapet Daily will fuel the curative response.
					</div>
					<div class="fadeInDown-2 text  hidden-xs" style="opacity: 0;">
					  Therapeutic levels of vitamins and minerals makes it a FOUNDATION for a complete Lifestyle.
					</div>
<!-- 					<div class="button-holder fadeInDown-3" style="opacity: 0;">
					    <a class="btn uppercase strong btn-secondary btn-add-to-cart btn-action" href="index.php?page=detail">Add To Cart</a>
					</div>
 -->					<!-- /.button-holder -->      
				</div><!-- /.caption -->   
			</div><!-- /.container --> 
		</div><!-- /.item -->

	</div><!-- /.owl-carousel -->
</div><!-- /#hero -->
			
<!-- ========================================= SECTION – HERO : END ========================================= -->