<div class="panel-group" id="product-detail-accordion">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">
                <a data-toggle="collapse" data-parent="#product-detail-accordion" href="#collapseDescription">
                    What's in it?
                    <span data-icon="&#x50;" class="expand pull-right"></span>
                    <span data-icon="&#x4f;" class="contract pull-right"></span>
                </a>
            </h3><!-- /.panel-title -->
        </div><!-- /.panel-heading -->
        <div id="collapseDescription" class="panel-collapse collapse in">
            <div class="panel-body">
                <ul class="list-kybully uppercase">
                    <li>Mega Pet Daily</li>
                    <li>Super C 2000</li>
                    <li>Yucca Intensive</li>
                    <li>Aller'G Free</li>
                </ul>
            </div><!-- /.panel-body -->
        </div><!-- /.panel-collapse -->
    </div><!-- /.panel-default -->

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">
                <a data-toggle="collapse" data-parent="#product-detail-accordion" href="#collapseShipping" class="collapsed">
                    Description
                    <span data-icon="&#x50;" class="expand pull-right"></span>
                    <span data-icon="&#x4f;" class="contract pull-right"></span>
                </a>
            </h3><!-- /.panel-title -->
        </div><!-- /.panel-heading -->
        <div id="collapseShipping" class="panel-collapse collapse">
            <div class="panel-body text-justify">
            A combination of all-natural nutrition supplements for your dog.             
            </div><!-- /.panel-body -->
        </div><!-- /.panel-collapse -->
    </div><!-- /.panel-default -->

<!--     <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">
                <a data-toggle="collapse" data-parent="#product-detail-accordion" href="#collapseFeatures" class="collapsed">
                    Features
                    <span data-icon="&#x50;" class="expand pull-right"></span>
                    <span data-icon="&#x4f;" class="contract pull-right"></span>
                </a>
            </h3>
 -->            <!-- /.panel-title -->
        <!-- </div> -->
        <!-- /.panel-heading -->
<!--         <div id="collapseFeatures" class="panel-collapse collapse">
            <div class="panel-body text-justify">
                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
            </div>
        </div>
 -->        <!-- /.panel-collapse -->
    <!-- </div> -->
    <!-- /.panel-default -->
</div><!-- /.panel-group -->